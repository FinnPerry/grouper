//! A library for generating links between items in a list.

#![warn(
    missing_docs,
    missing_debug_implementations,
    clippy::unwrap_used,
    clippy::missing_docs_in_private_items,
    clippy::pedantic
)]

use std::ops::Range;

/// Trait for types that may be linked to other instances of the type.
pub trait Linkable {
    /// Check if an item can link to another.
    fn can_link_to(&self, to: &Self) -> bool;
}

/// An item and info about how the item is linked.
#[derive(Debug, Clone)]
struct LinkedItem<T> {
    /// The item's value.
    item: T,
    /// Is this item linked to by another item.
    is_taken: bool,
    /// The index of the item that this item is linked to.
    link: Option<usize>,
}

impl<T> From<T> for LinkedItem<T> {
    fn from(item: T) -> Self {
        Self {
            item,
            is_taken: false,
            link: None,
        }
    }
}

/// A list of links between items.
#[derive(Debug, Clone)]
pub struct LinkVec<T> {
    /// The list of items.
    items: Vec<LinkedItem<T>>,
}

/// Errors that may occur in grouper functions.
#[derive(Debug, thiserror::Error, PartialEq, Eq)]
#[non_exhaustive]
pub enum Error {
    /// The rng function produced a value that was outside of the requested range.
    #[error("rng function produced an out of bounds value")]
    OutOfBoundsRng(Range<usize>, usize),
}

/// Result type for fallable grouper functions.
pub type Result<T> = std::result::Result<T, Error>;

impl<T> LinkVec<T> {
    /// Reset the links.
    fn reset(&mut self) {
        for i in &mut self.items {
            i.is_taken = false;
            i.link = None;
        }
    }

    /// Check if the list contains any links to none.
    fn contains_none(&self) -> bool {
        self.items.iter().any(|i| i.link.is_none())
    }
}

impl<T> LinkVec<T>
where
    T: Linkable,
{
    /// Create a new `LinkVec` and generate the links.
    /// Randomness is supplied by the caller via the `rng` fn.
    ///
    /// # Errors
    ///
    /// Returns an error if the rng function produced a value that was outside of the requested range.
    pub fn new<F>(items: Vec<T>, retries: u32, rng: &F) -> Result<Self>
    where
        F: Fn(Range<usize>) -> usize,
    {
        let mut list: Self;
        list = Self {
            items: items.into_iter().map(Into::into).collect(),
        };
        for _ in 0..=retries {
            list.reset();
            list.mk_links(&rng)?;
            if !list.contains_none() {
                return Ok(list);
            }
        }
        Ok(list)
    }

    /// Get an iterator over the generated links.
    pub fn iter(&self) -> impl Iterator<Item = (&T, Option<&T>)> {
        self.items.iter().map(|item| {
            let a = &item.item;
            let b_index = item.link;
            let b_item = b_index.and_then(|i| self.items.get(i));
            let b = b_item.map(|b_item| &b_item.item);
            (a, b)
        })
    }

    /// Generate links.
    fn mk_links<F>(&mut self, rng: &F) -> Result<()>
    where
        F: Fn(Range<usize>) -> usize,
    {
        // Randomize item order.
        let indices = {
            let mut indices: Vec<usize> = (0..self.items.len()).collect();
            let mut rand_indices = Vec::with_capacity(indices.len());
            while !indices.is_empty() {
                let r = checked_rng(rng, 0..indices.len())?;
                rand_indices.push(indices[r]);
                indices.remove(r);
            }
            rand_indices
        };
        // Generate links.
        for i in indices {
            self.link_item(i, rng)?;
        }
        Ok(())
    }

    /// Make a random link for an item.
    fn link_item<F>(&mut self, i: usize, rng: &F) -> Result<()>
    where
        F: Fn(Range<usize>) -> usize,
    {
        let untaken = self.get_eligible_for(i);
        if !untaken.is_empty() {
            let r = checked_rng(rng, 0..untaken.len())?;
            let j = untaken[r];
            self.items[j].is_taken = true;
            self.items[i].link = j.into();
        }
        Ok(())
    }

    /// Get a list of indices to items who can be linked with an item.
    fn get_eligible_for(&self, i: usize) -> Vec<usize> {
        let item = &self.items[i];
        self.items
            .iter()
            .enumerate()
            .filter(|j| !j.1.is_taken)
            .filter(|j| j.1.item.can_link_to(&item.item))
            .filter(|j| j.0 != i)
            .map(|j| j.0)
            .collect()
    }
}

/// Produce a value from the given rng function, and ensure the value is in the requested range.
fn checked_rng<F>(rng: &F, range: Range<usize>) -> Result<usize>
where
    F: Fn(Range<usize>) -> usize,
{
    let r = rng(range.clone());
    if range.contains(&r) {
        Ok(r)
    } else {
        Err(Error::OutOfBoundsRng(range, r))
    }
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod test {
    use super::*;

    struct TestLinkable(&'static str);

    impl Linkable for TestLinkable {
        fn can_link_to(&self, to: &Self) -> bool {
            let self_first_char = self.0.chars().nth(0);
            let other_first_char = to.0.chars().nth(0);
            self_first_char == other_first_char
        }
    }

    #[test]
    fn contains_none() {
        let items = vec!["a".into()];
        let list = LinkVec::<&str> { items };
        assert!(list.contains_none());
    }

    #[test]
    fn not_contains_none() {
        let items = vec![];
        let list = LinkVec::<&str> { items };
        assert!(!list.contains_none());
    }

    #[test]
    fn reset_links() {
        let items = vec!["a".into(), "b".into()];
        let mut list = LinkVec::<&str> { items };
        list.reset();
        assert_eq!(list.items.len(), 2);
        assert!(list.items.iter().all(|i| !i.is_taken));
        assert!(list.items.iter().all(|i| i.link.is_none()));
    }

    #[test]
    fn get_eligible_for() {
        let items = vec![
            TestLinkable("a").into(),
            TestLinkable("a").into(),
            TestLinkable("b").into(),
        ];
        let list = LinkVec::<TestLinkable> { items };
        assert_eq!(list.get_eligible_for(0), [1]);
        assert_eq!(list.get_eligible_for(1), [0]);
        assert_eq!(list.get_eligible_for(2), []);
    }

    #[test]
    fn link_item() {
        let items = vec![TestLinkable("a").into(), TestLinkable("a").into()];
        let mut list = LinkVec::<TestLinkable> { items };
        list.reset();

        let rng = |r: Range<usize>| r.start;
        list.link_item(0, &rng).unwrap();

        assert_eq!(list.items[0].link.unwrap(), 1);
        assert!(list.items[1].is_taken);
    }

    #[test]
    fn mk_links() {
        let items = vec![TestLinkable("a").into(), TestLinkable("a").into()];
        let mut list = LinkVec::<TestLinkable> { items };
        list.reset();

        let rng = |r: Range<usize>| r.start;
        list.mk_links(&rng).unwrap();

        assert_eq!(list.items[0].link.unwrap(), 1);
        assert!(list.items[1].is_taken);

        assert_eq!(list.items[1].link.unwrap(), 0);
        assert!(list.items[0].is_taken);
    }

    #[test]
    fn new_all_linked() {
        let items = vec![TestLinkable("a"), TestLinkable("a")];
        let rng = |r: Range<usize>| r.start;
        let list = LinkVec::<TestLinkable>::new(items, 0, &rng).unwrap();

        assert_eq!(list.items[0].link.unwrap(), 1);
        assert!(list.items[1].is_taken);

        assert_eq!(list.items[1].link.unwrap(), 0);
        assert!(list.items[0].is_taken);
    }

    #[test]
    fn new_none_linked() {
        let items = vec![TestLinkable("a")];
        let rng = |r: Range<usize>| r.start;
        let list = LinkVec::<TestLinkable>::new(items, 0, &rng).unwrap();

        assert!(list.items[0].link.is_none());
    }

    #[test]
    fn checked_rng_ok() {
        let rng = |r: Range<usize>| r.start;
        assert_eq!(checked_rng(&rng, 0..1).unwrap(), 0);
    }

    #[test]
    fn checked_rng_err() {
        let rng = |_: Range<usize>| usize::MAX;
        assert_eq!(
            checked_rng(&rng, 0..1).unwrap_err(),
            Error::OutOfBoundsRng(0..1, usize::MAX)
        );
    }

    #[test]
    fn iter() {
        let items = vec![TestLinkable("a1"), TestLinkable("a2")];
        let rng = |r: Range<usize>| r.start;
        let list = LinkVec::<TestLinkable>::new(items, 0, &rng).unwrap();

        let links: Vec<_> = list.iter().collect();
        assert_eq!(links.len(), 2);

        assert_eq!(links[0].0 .0, "a1");
        assert_eq!(links[0].1.unwrap().0, "a2");

        assert_eq!(links[1].0 .0, "a2");
        assert_eq!(links[1].1.unwrap().0, "a1");
    }
}
